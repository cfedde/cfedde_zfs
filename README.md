# cfedde_zfs

Notes about zfs on my local network.

## root & boot on bigbox

The root and boot pools are as configured by the ubuntu install process. these are both partitions on the same nvme ssd.

```
cfedde@bigbox:~$ zpool status -L rpool bpool
  pool: bpool
 state: ONLINE
  scan: scrub repaired 0B in 00:00:00 with 0 errors on Sun Feb 12 00:24:01 2023
config:

	NAME         STATE     READ WRITE CKSUM
	bpool        ONLINE       0     0     0
	  nvme0n1p3  ONLINE       0     0     0

errors: No known data errors

  pool: rpool
 state: ONLINE
  scan: scrub repaired 0B in 00:01:21 with 0 errors on Sun Feb 12 00:25:22 2023
config:

	NAME         STATE     READ WRITE CKSUM
	rpool        ONLINE       0     0     0
	  nvme0n1p4  ONLINE       0     0     0

errors: No known data errors

```
# tank

Tank is the zpool that contians plain old user disks
```
13:33:21 cfedde@theseus:~/src/ghgt-zfs
$ zpool status tank
  pool: tank
 state: ONLINE
  scan: scrub repaired 0B in 00:13:03 with 0 errors on Sun Apr  9 00:37:04 2023
config:

	NAME                                 STATE     READ WRITE CKSUM
	tank                                 ONLINE       0     0     0
	  raidz1-0                           ONLINE       0     0     0
	    ata-ST2000DM008-2FR102_ZFL262HG  ONLINE       0     0     0
	    ata-ST2000DM008-2FR102_ZFL29GZA  ONLINE       0     0     0
	    ata-ST2000DM008-2FR102_ZFL2BBYB  ONLINE       0     0     0
	    ata-ST2000DM008-2FR102_ZK201RJW  ONLINE       0     0     0

errors: No known data errors
```

And on bigbox:
```
13:38:33 cfedde@bigbox:~
$ zpool status tank
  pool: tank
 state: ONLINE
  scan: scrub repaired 0B in 00:11:21 with 0 errors on Sun Apr  9 00:35:26 2023
config:

	NAME                                 STATE     READ WRITE CKSUM
	tank                                 ONLINE       0     0     0
	  raidz1-0                           ONLINE       0     0     0
	    ata-ST2000LM015-2E8174_ZDZD521S  ONLINE       0     0     0
	    ata-ST2000LM015-2E8174_ZDZD529D  ONLINE       0     0     0
	    ata-ST2000LM015-2E8174_ZDZD52Z2  ONLINE       0     0     0
	    ata-ST2000LM015-2E8174_ZDZD59YJ  ONLINE       0     0     0
	logs	
	  loop30                             ONLINE       0     0     0

errors: No known data errors
```

## ZIL on bigbox

On bigbox I use a sketchy loop back device on the root zfs file system as a zil.
The loop device will be different with each boot. 
Writes to the spinning media are kind of slow. And write rates to the root pool is pretty low so a zil in a loop
device seemed expedient.

```
sudo losetup --find --show /mnt/zil
sudo zpool import tank
```
This needs to happen before zfs is able the tank pool and all it's file systems available.
So far I have not been able to teach systemd to do this for me.
