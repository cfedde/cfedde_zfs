#!/usr/bin/env bash

set -x
set -a
set -e
set -u
set -o pipefail

unit=cfedde-zil-loop.service
function install {
    cp $unit /etc/systemd/system
    systemctl enable $unit
}

function remove {
    systemctl stop $unit
    systemctl disable $unit
    rm /etc/systemd/system/$unit
}

"${1:-install}"
