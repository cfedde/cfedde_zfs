# Using Many Partions 

The idea here is to divide physical disk drives into a number of equal partitions then group them up into vdevs to build zpools.

Setup:

```
   sda        sdb         sdc        sdd
  +-------+  +-------+  +-------+  +-------+   
  | p0    |  | p0    |  | p0    |  | p0    |  
  | p1    |  | p1    |  | p1    |  | p1    |  
  | p2    |  | p2    |  | p2    |  | p2    |  
  | p3    |  | p3    |  | p3    |  | p3    |  
  +-------+  +-------+  +-------+  +-------+  
```

Four equal 4Tbyte drives.  Each with four 1T partitions.
Make four vdevs:

- sd[a,b,c,d}-p0
- sd[a,b,c,d}-p1
- sd[a,b,c,d}-p2
- sd[a,b,c,d}-p3

Then add all these to a zpool: tank

16 partitions
4 vdev 


# Add an 8T disk

```
   sda        sdb         sdc         sdd       sde
  +-------+  +-------+  +-------+  +-------+  +-------+   
  | p0    |  | p0    |  | p0    |  | p0    |  | p0    |  
  | p1    |  | p1    |  | p1    |  | p1    |  | p1    |  
  | p2    |  | p2    |  | p2    |  | p2    |  | p2    |  
  | p3    |  | p3    |  | p3    |  | p3    |  | p3    |  
  +-------+  +-------+  +-------+  +-------+  | p4    | 
                                              | p5    |
                                              | p6    | 
                                              | p7    |
                                              +-------+
```

Reorganize data:

Maintain 4 1T partions per vdev.
no two partitions in a vdev on the same drive

24 partitions
5 vdevs


